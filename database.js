const mysql = require('mysql');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('letsHopeItDoesntGetLeaked');

var userTable = "gebot.users";

// Query
// create table users( user_id varchar(40) primary key, p_status decimal(5,2) default -1, lang varchar(10), address varchar(120), phone varchar(60), email varchar(100), a_long varchar(10), a_lat varchar(10), pincode varchar(10));
// create index user_index on gebot.users(user_id, lang, p_status);

var colUserId = "user_id";
var colProfileStatus = "p_status"; // Status -1 default
var colLang = "lang";       // Status 0
var colAddress = "address"; // Status 1
var colPincode = "pincode"; // Status 2
var colPhone = "phone";     // Status 3
var colMail = "email";      // Status 4     - 42 char
var colLong = "a_long";     // Status 5
var colLat = "a_lat";       // Status 5

var con = mysql.createConnection({
    host: "localhost",
    user: "msahil432",
    password: "1504"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected to db!");
});

function getLang(user_id, callback){
    var query = `select ${colLang} from ${userTable} where ${colUserId} = \"${user_id}\";`;
    console.log(query);
    con.query(query, function(err, result){
        if(err){
            console.log(err);
            return 'en';
        }
        if(result[0]) return result[0][colLang];
        return 'en';
    });
}

function getUProfileStatus(user_id, callback){
    var query = `select ${colProfileStatus} from ${userTable} where ${colUserId} = \"${user_id}\";`;
    console.log(query);
    con.query(query, function(err, result){
        if(err){
            console.log(err);
            callback(-1);
        }
        if(result[0])   callback(result[0][colProfileStatus]);
        else    callback(-1);
    })
}

function getUser(user_id, callback){
    var query = `select * from ${userTable} where ${colUserId} = \"${user_id}\";`;
    console.log(query);
    con.query(query, function(err, result){
        if(err){
            console.log(err);
            callback(undefined);
        }
        if(result[0]){
            if(result[0][colPhone])
                result[0][colPhone] = cryptr.decrypt(result[0][colPhone]);
            if(result[0][colMail])
                result[0][colMail] = cryptr.decrypt(result[0][colMail]);
            callback(result[0]);
        }
        else    callback(undefined);
    })
}

function setLanguage(user_id, lang){
    var query = `insert into ${userTable}(${colUserId}, ${colLang}, ${colProfileStatus}) values (\"${user_id}\", \"${lang}\", 0) on duplicate key update ${colLang}=\"${lang}\";`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    })
}

function setProfileStatus(user_id, status){
    var q = `update ${userTable} set ${colProfileStatus}=${status} where ${colUserId}=\"${user_id}\";`;
    console.log(q);
    con.query(q, function(err){
        if(err) console.log(err);
    })
}

function setUAddress(user_id, address){
    var query = `update ${userTable} set ${colAddress}=\"${address}\", ${colProfileStatus}=1 where ${colUserId}=\"${user_id}\";`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    })
}

function setUPincode(user_id, pincode){
    var query = `update ${userTable} set ${colPincode}=\"${pincode}\", ${colProfileStatus}=2 where ${colUserId}=\"${user_id}\";`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    })
}

function setUPhone(user_id, phone){
    phone = cryptr.encrypt(phone);
    var query = `update ${userTable} set ${colPhone}=\"${phone}\", ${colProfileStatus}=3 where ${colUserId}=\"${user_id}\";`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    })
}

function setUEmail(user_id, email){
    email = cryptr.encrypt(email);
    var query = `update ${userTable} set ${colMail}=\"${email}\", ${colProfileStatus}=4 where ${colUserId}=\"${user_id}\";`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    })
}

function setULocation(user_id, lat, long){
    var query = `update ${userTable} set ${colLong}=\"${long}\", ${colLat}=\"${lat}\", ${colProfileStatus}=5 where ${colUserId}=\"${user_id}\";`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    })
}

var servicesTable = "gebot.services";
// Create Query
// create table services(service_id int auto_increment primary key, issue varchar(300), user_id varchar(20), agent_id varchar(20), s_date date, timeslot int default -1, status int default -1, pincode varchar(10), foreign key (user_id) references users(user_id));
// create index service_index on gebot.services(service_id, user_id, agent_id);

var coServiceId = "service_id";
var coIssue = "issue";
var coPincode = "pincode";
var coAgentId = "agent_id";
var coUserId = "user_id";
var coDate = "s_date";
var coTimeSlot = "timeslot";    // -1: Unset & default, 1: Morning, 2: Noon, 3: Evening
var coServiceStatus = "status"; // -1 : unfinished, 0: Initiated, 1: Confirmed, 2: Done

function createRequest(user_id, issue, pincode, callback){
    var query = `insert into ${servicesTable}(${coUserId}, ${coIssue}, ${coPincode}) values(\"${user_id}\", \"${issue}\", \"${pincode}\");`;
    console.log(query);
    con.query(query, function(err){
        if(err){
            console.log(err);
            callback(undefined);
        }
        else{
            var q = `select ${coServiceId} from ${servicesTable} where ${coUserId}=\"${user_id}\" order by ${coServiceId} desc;`;
            console.log(q);
            con.query(q, function(err, result){
                if(err){
                    console.log(err);
                    callback(undefined);
                }else callback(result[0][coServiceId]);
            });
        }
    });
}

function setSDate(service_id, date, callback){
    var q = `update ${servicesTable} set ${coDate}=\"${date} where ${coServiceId}=${service_id};`;
    console.log(q);
    con.query(q, function(e){
        if(e){
            console.log(e);
            callback(-1);
            return;
        }
        callback(1);
    });
}

function setTimeslot(service_id,timeslot){
    var q = `update ${servicesTable} set ${coTimeSlot}=${timeslot}, ${coServiceStatus}=0 where ${coServiceId}=${service_id};`;
    console.log(q);
    con.query(q, function(e){
        if(e) console.log(e);
    });
}

function setServiceAgent(service_id, agent_id){
    var query = `update ${servicesTable} set ${coAgentId}=\"${agent_id}\" where ${coServiceId}=${service_id}`;
    console.log(query);
    con.query(query, function(err){
        if(err) console.log(err);
    });
}

function getDetails(service_id, callback){
    var q = `select * from ${servicesTable} where ${coServiceId}=${service_id};`;
    con.query(q, function(e, r){
        if(e){
            console.log(e);
            callback(undefined);
        }else   callback(r[0]);
    })
}

function getServices(user_id, callback){
    var q = `select * from ${servicesTable} where ${coUserId}=${user_id} order by ${coServiceId} desc;`;
    con.query(q, function(e, r){
        if(e){
            console.log(e);
            callback(undefined);
        }else   callback(r);
    })
}

module.exports = {
    user:{
        getLang,
        getUProfileStatus,
        getUser,
        setLanguage,
        setProfileStatus,
        setUAddress,
        setUPincode,
        setUPhone,
        setUEmail,
        setULocation
    },
    service:{
        createRequest,
        setSDate,
        setTimeslot,
        setServiceAgent,
        getDetails,
        getServices
    },
}