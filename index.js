'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const strings = require('./strings');
const db = require('./database');
const convo = require('./convo');

const app = express();
app.set('port', (process.env.PORT || 5000));

// Process application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// Process application/json
app.use(bodyParser.json());

// Index route
app.get('/', function (req, res) {
    res.send('Hello world!');
})

// for Facebook verification
app.get('/webhook/', function (req, res){
	if (req.query['hub.verify_token'] === 'GE_2018_SERVICE_CHAT_BOT'){
		res.send(req.query['hub.challenge']);
	}
	res.send('Error, wrong token');
});

// Spin up the server
app.listen(process.env.port || 5000, function(){	
	console.log('running on port', process.env.port || 5000);
});

//message identification
app.post('/webhook/', function (req, res){
	let messaging_events = req.body.entry[0].messaging;
	for (let i = 0; i < messaging_events.length; i++){
		let event = req.body.entry[0].messaging[i];
		let sender = event.sender.id;
		try{
			sendTyping(sender);
			if(event.message["quick_reply"])
				handlePostbacks(sender, event.message["quick_reply"]["payload"]);
			else if(event.message.attachments && event.message.attachments[0].type == 'location')
				saveLocation(sender, event.message.attachments[0].payload.coordinates);
			else if (event.message && event.message.text)
				handleFirstMessage(sender, event.message.text);
			else
				console.log("Unknown Message from "+sender, event);
		}catch(e){
			try{sendTypingOff(sender);}catch(e){}
			console.log(sender, event, e);
		}
	}
	res.sendStatus(200);
});

//page token
const token = "EAAM1QY2CliYBALHowDNOiWw9xZCskBL1AAs1SCxl2EaSb8ABtIJj6XhlQhw5GZBDa1iXC0esZCzZA3HZAiyg8ePPCtiS390fsZCJsPFltrFV6hXRd64RWrxG9FqyODPDh0Pd5YaKjk9j5OqZAdwcwT4GeOJCpIhHVbh9fCXEDeaVFt61e22JmZCv"

function send(reciever, msgData, comment){
	console.log(reciever+' sending to '+comment, msgData);
	request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token:token},
        method: 'POST',
        json: {
            recipient: {id:reciever},
            message: msgData,
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Sending Error '+comment, error);
        } else if (response.body.error) {
            console.log('Error: '+comment, response.body.error);
        }
    });
}

function sendTyping(reciever){
	request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token:token},
        method: 'POST',
        json: {
            "recipient": {"id":reciever},
            "sender_action":"typing_on",
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Sending Error of typing on', error);
        } else if (response.body.error) {
            console.log('Error of typing on: ', response.body.error);
        }
    });
}

function sendTypingOff(reciever){
	request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token:token},
        method: 'POST',
        json: {
            "recipient": {"id":reciever},
            "sender_action":"typing_off",
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Sending Error of typing on', error);
        } else if (response.body.error) {
            console.log('Error of typing on: ', response.body.error);
        }
    });
}

function sendTextMessage(sender, text) {
    let messageData = { text:text }
    send(sender, messageData, "simple text msg");
}

function sendTextWithReplies(sender, text, replies){
	let t = {
		text : text,
		quick_replies : []
	};
	for(var i=0; i<replies.length; i++){
		t.quick_replies[i] = replies[i];
	}
	send(sender, t, "text with replies "+replies.length);
}

function createReply(title, type, payload){
	return {
		title: title,
		content_type: type,
		payload: payload
	};
}

function sendCards(sender, cards){
	var messageData = {
		"attachment" :{
	        "type" : "template",
	        "payload" : {
	        	"template_type" : "generic",
	        	"elements" :[]
			}
	    }
	};
	for(var i=0; i<cards.length; i++){
		messageData.attachment.payload.elements[i] = cards[i];
	}
	send(sender,messageData, "cards "+cards.length);
}

function createCard(title, image_url, subtitle, text, btn1, btn2, btn3){
	var temp = {
		"title" : title,
		"buttons" :[]
	};
	if(image_url)	temp["image_url"] = image_url;
	if(subtitle)	temp["subtitle"] = subtitle;
	if(text)	temp["text"] = text;
	if(btn1)	temp.buttons[0] = btn1;
	if(btn2)	temp.buttons[1] = btn2;
	if(btn3)	temp.buttons[2] = btn3;
	return temp;
}

function createPostback(title, type, payload){
	var t = {
		"type" : type,
		"payload" : payload,
		"title" : title
	};
	return t;
}

var languagesReplies = [
	createReply("English", "text", "lang-en")
];

var tasksAvailable = [
	createReply("Book a service", "text", "service-new"),
	createReply("Track your service", "text", "service-track")
];

var locationReply = {"content_type":"location"};
var timeReplies = [
	createReply("10AM - 1PM", "text", "service-timeslot-1"),
	createReply("2PM - 5PM", "text", "service-timeslot-2"),
	createReply("5PM - 8PM", "text", "service-timeslot-3")
];

function handleFirstMessage(sender, text){
	console.log("message recieved:", text);
	db.user.getUProfileStatus(sender, function(r){
		if(r==-1){
			sendTextWithReplies(sender, convo.youAreNew(sender), languagesReplies);
			return;
		}else{
			db.user.getUser(sender, function(profile){
				if(profile.p_status<5){
					completeProfile(sender, profile, text);
				}else if(profile.p_status>9){
					completeIssue(profile, text);
				}else if(profile.p_status>20){
					trackRequest(profile, text);
				}else{
					console.log("reached here");
					sendTextWithReplies(sender, convo.hello(sender), tasksAvailable);
				}
			})
		}
	})
}

function saveLocation(sender, cors){
	sendTextWithReplies(sender, convo.saved(sender), tasksAvailable);
	db.user.setULocation(sender, cors.lat, cors.long);
}

function handlePostbacks(sender, payload){
	console.log("\nPostback recieved ",payload);
	var strs = payload.split("-");
	switch(strs[0]){
		case "lang":{
			manageLang(sender, strs);
			break;
		}
		case "service":{
			manageService(sender, strs);
			break;
		}
	}
}

function manageService(sender, strs){
	switch(strs[1]){
		case "new":{
			sendTextMessage(sender, convo.explainIssue(sender));
			db.user.setProfileStatus(sender, 10.5);
			break;
		}
		case "timeslot":{
			db.service.getServices(sender, function(srs){
				var last = srs[0];
				db.service.setTimeslot(last.service_id, strs[2]);
				sendTextMessage(sender, convo.requestCreated(sender)+last.service_id);
				db.user.setProfileStatus(sender, 5);
			});
			break;
		}
		case "track":{
			sendTextMessage(sender, convo.chooseTrackOrEnter(sender));
			db.user.setProfileStatus(sender, 21);
			break;
		}
		case "request":{
			trackRequest(sender, strs[1]);
		}
	}
}

function trackRequest(sender, text){
	db.service.getDetails(text, function(service){
		db.user.setProfileStatus(sender, 5);
		var state;
		switch(service.status){
			case 0:{state = convo.initiated(sender); break;}
			case 1:{state = convo.confirmed(sender); break;}
			case 2:{state = convo.completed(sender); break;}
			default:{state = convo.unknown(sender)}
		}
		var dateAndTime = service.s_date+" ";
		switch(service.timeslot){
			case 0:{dateAndTime+= "10AM - 1PM"; break;}
			case 1:{dateAndTime+= "2AM - 5PM"; break;}
			case 2:{dateAndTime+= "5PM - 8PM"; break;}
			default:{dateAndTime+= convo.unknown(sender)}
		}
		sendTextMessage(sender,
			convo.trackDetails(sender)+"\n"
			+convo.trackingId(sender)+text+"\n"
			+convo.status(sender)+state+"\n"
			+convo.agentId(sender)+service.agent_id+"\n"
			+convo.dateAndTime(sender)+dateAndTime
		);
	});
}

function completeIssue(profile, text){
	if(profile.p_status == 10.5){
		db.service.createRequest(profile.user_id, text, profile.pincode, function(id){
			sendTextMessage(profile.user_id, convo.serviceDate(profile.user_id));
			db.user.setProfileStatus(profile.user_id, 11.5);
		});
		sendTextMessage(profile.user_id, convo.saved(profile.user_id));
		return;
	}
	db.service.getServices(profile.user_id, function(srs){
		var last = srs[0];
		if(profile.p_status==11.5){
			db.service.setSDate(last.service_id, text, function(result){
				if(result!=1){
					sendTextMessage(profile.user_id, convo.serviceDate(profile.user_id));
				}else{
					sendTextWithReplies(profile.user_id, convo.chooseTimeSlot(profile.user_id), timeReplies);
					db.user.setProfileStatus(profile.user_id, 12.5);
				}
			});
		}
	});
}

function completeProfile(sender, profile, text){
	switch(profile.p_status){
		case 0.5:{
			db.user.setUAddress(sender, text);
			sendTextMessage(sender, convo.saved(sender));
			break;
		}
		case 1.5:{
			db.user.setUPincode(sender, text);
			sendTextMessage(sender, convo.saved(sender));
			break;
		}
		case 2.5:{
			db.user.setUPhone(sender, text);
			sendTextMessage(sender, convo.saved(sender));
			break;
		}
		case 3.5:{
			db.user.setUEmail(sender, text);
			sendTextMessage(sender, convo.saved(sender));
			break;
		}
	}
	if(profile.p_status==0 && !profile.address){
		sendTextMessage(sender, convo.profileAddress(sender));
		db.user.setProfileStatus(sender, 0.5);
	}else if(profile.p_status==1 && !profile.pincode){
		sendTextMessage(sender, convo.pincode(sender));
		db.user.setProfileStatus(sender, 1.5);
	}else if(profile.p_status==2 && !profile.phone){
		sendTextMessage(sender, convo.phone(sender));
		db.user.setProfileStatus(sender, 2.5);
	}else if(profile.p_status==3 && !profile.email){
		sendTextMessage(sender, convo.email(sender));
		db.user.setProfileStatus(sender, 3.5);
	}else if(profile.p_status==4 && !profile.a_long){
		sendTextWithReplies(sender, convo.lastStepLocation(sender), [locationReply]);
	}
}

function manageLang(sender, strs){
	switch(strs[1]){
		default :{
			db.user.setLanguage(sender, 'en');
			break;
		}
	}
	sendTextMessage(sender, convo.langSaved(sender));
}