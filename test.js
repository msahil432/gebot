const convo = require('./convo');
const db = require('./database');

// db.user.setUEmail("1504", "1234567890123456789012345678901@outlook.in");

// db.user.getUser("1504", function(res){
//     console.log("Working : ",res);
// });

// console.log("Say Hello : "+convo.greet("1504","sahil"));

db.service.getDetails("1", function(service){
    var text = "1";
    var sender = "1504";
    db.user.setProfileStatus(sender, 5);
    var state;
    switch(service.status){
        case 0:{state = convo.initiated(sender); break;}
        case 1:{state = convo.confirmed(sender); break;}
        case 2:{state = convo.completed(sender); break;}
        default:{state = convo.unknown(sender)}
    }
    var dateAndTime = service.s_date+" ";
    switch(service.timeslot){
        case 0:{dateAndTime+= "10AM - 1PM"; break;}
        case 1:{dateAndTime+= "2AM - 5PM"; break;}
        case 2:{dateAndTime+= "5PM - 8PM"; break;}
        default:{dateAndTime+= convo.unknown(sender)}
    }
    console.log(
        convo.trackDetails(sender)+"\n"
        +convo.trackingId(sender)+text+"\n"
        +convo.status(sender)+state+"\n"
        +convo.agentId(sender)+service.agent_id+"\n"
        +convo.dateAndTime(sender)+dateAndTime
    );
});