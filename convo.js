const db = require('./database');

var strings;

function setStrings(user_id){
    switch(db.user.getLang(user_id)){
        default:{
            strings = require('./strings');
            break;
        }
    }
}

function hello(user_id){
    setStrings(user_id);
    return strings.helloHelp;
}

function greet(user_id, name){
    setStrings(user_id);
    return strings.hello+" "+name;
}

function youAreNew(user_id){
    setStrings(user_id);
    return strings.youAreNew;
}

function langSaved(suer_id){
    setStrings(suer_id);
    return strings.langaugeSaved;
}

function loginRequired(user_id){
    setStrings(user_id);
    return strings.loginRequired;
}

function inCompleteSetup(user_id){
    setStrings(user_id);
    return strings.inCompleteSetup;
}

function locationString(user_id){
    setStrings(user_id);
    return strings.locationString;
}

function addressString(user_id){
    setStrings(user_id);
    return strings.addressString;
}

function phoneString(user_id){
    setStrings(user_id);
    return strings.phoneString;
}

function profileAddress(user_id){
    setStrings(user_id);
    return strings.profileAddress;
}

function pincode(user_id){
    setStrings(user_id);
    return strings.pincode;
}

function phone(user_id){
    setStrings(user_id);
    return strings.phone;
}

function email(user_id){
    setStrings(user_id);
    return strings.email;
}

function lastStepLocation(user_id){
    setStrings(user_id);
    return strings.lastStepLocation;
}

function saved(user_id){
    setStrings(user_id);
    return strings.saved;
}

function explainIssue(user_id){
    setStrings(user_id);
    return strings.explainIssue;
}

function serviceDate(user_id){
    setStrings(user_id);
    return strings.serviceDate;
}

function chooseTimeSlot(user_id){
    setStrings(user_id);
    return strings.chooseTimeSlot;
}

function requestCreated(user_id){
    setStrings(user_id);
    return strings.requestCreated;
}

function chooseTrackOrEnter(user_id){
    setStrings(user_id);
    return strings.chooseTrackOrEnter;
}

function trackDetails(user_id){
    setStrings(user_id);
    return strings.trackDetails;
}

function initiated(user_id){
    setStrings(user_id);
    return strings.initiated;
}

function confirmed(user_id){
    setStrings(user_id);
    return strings.confirmed;
}

function completed(user_id){
    setStrings(user_id);
    return strings.completed;
}

function unknown(user_id){
    setStrings(user_id);
    return strings.unknown;
}

function trackingId(user_id){
    setStrings(user_id);
    return strings.trackingId;
}

function status(user_id){
    setStrings(user_id);
    return strings.status;
}

function agentId(user_id){
    setStrings(user_id);
    return strings.agentId;
}

function dateAndTime(user_id){
    setStrings(user_id);
    return strings.dateAndTime;
}

module.exports = {
    hello,
    greet,
    langSaved,
    loginRequired,
    pincode,
    saved,
    phone,
    email,
    lastStepLocation,
    inCompleteSetup,
    locationString,
    addressString,
    phoneString,
    youAreNew,
    profileAddress,

    explainIssue,
    serviceDate,
    chooseTimeSlot,
    requestCreated,
    chooseTrackOrEnter,
    trackDetails,
    trackingId,
    unknown,
    completed,
    confirmed,
    initiated,
    status,
    agentId,
    dateAndTime
};